#!/usr/bin/env ruby
#encoding: utf-8

require 'rubygems'
require 'date'
require 'json'
require 'rest-client'

class ParkingSpot
  
  attr_accessor :id, :uuid
  attr_accessor :lat, :lon, :description, :capabilities, :status
  attr_accessor :availability_schedules, :spot_availability

  def initialize(params={})
    params.each do |key, value|
      instance_variable_set("@#{key}", value)
    end
    self.status = "active"
    self.description = "A Parking Spot"
    self.capabilities = [
      "spot_availability",
      "availability_schedules"
    ]
  end

  def data
    {
      lat: self.lat,
      lon: self.lon,
      description: self.description,
      capabilities: self.capabilities,
      status: self.status
    }
  end

  def register
    begin
      puts self.data
      response = RestClient.post(
        ENV["ADAPTOR_HOST"] + "/components",
        {data: self.data}
      )
      self.uuid = JSON.parse(response.body)['data']['uuid']
      return true
    rescue RestClient::Exception => e
      puts "Could not register resource: #{e.response}"
      return false
    end
  end

  def change_availability
    if self.spot_availability == 1
      self.spot_availability = 0
    else
      self.spot_availability = 1
    end
  end

  def send_data(capability_name = :spot_availability)
    timestamp = DateTime.now.to_s
    capability = capability_name.to_s
    value = self.send(capability_name)

    data_json = {}
    data_json[capability] = [{value: value, timestamp: timestamp}]
    begin
      response = RestClient.post(
        ENV["ADAPTOR_HOST"] + "/components/#{self.uuid}/data",
        {data: data_json}
      )
      puts "Success in post #{capability} observation"
    rescue RestClient::Exception => e
      puts "Could not send data: #{e.response}"
    end
  end
end

