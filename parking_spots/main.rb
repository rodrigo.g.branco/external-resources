##
# Create components for each parking spot that will be simulated.
#

require 'yaml'
require_relative 'parking_spot'

data = YAML.load(File.read(File.join(File.dirname(__FILE__), "parking_spots.yml")))

puts 'Importing parking spots data:'

spots = []

data['spots'].each do |spot|
  parking_spot = ParkingSpot.new(
    lat: spot['latitude'],
    lon: spot['longitude'],
  )

  parking_spot.availability_schedules =
    spot['availability_schedules'].to_json

  parking_spot.spot_availability = 0

  if parking_spot.register
    print '.'
    spots << parking_spot
    parking_spot.send_data(:availability_schedules)
    parking_spot.send_data(:spot_availability)
  else
    print 'F'
  end
end

puts
puts "#{spots} spots!"

while true do
  changed = (1..25).to_a.sample
  changed.times do
    spot = spots.sample
    spot.change_availability
    spot.send_data(:spot_availability)
  end
  sleep 4
end
